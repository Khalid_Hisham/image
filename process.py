import glob
import os
from PIL import Image, ImageOps, ImageFilter

logo = Image.open('logo.jpg')
logo2 = Image.open('logo2.jpg')

def drop(logo):
    RADIUS = 10

    # Paste image on white background
    diam = 2 * RADIUS
    back = Image.new('RGB', (logo.size[0] + diam, logo.size[1] + diam), (255, 255, 255))
    back.paste(logo, (RADIUS, RADIUS))

    # Create blur mask
    mask = Image.new('L', (logo.size[0] + diam, logo.size[1] + diam), 255)
    blck = Image.new('L', (logo.size[0] - diam, logo.size[1] - diam), 0)
    mask.paste(blck, (diam, diam))

    # Blur image and paste blurred edge according to mask
    blur = back.filter(ImageFilter.GaussianBlur(RADIUS / 2))
    back.paste(blur, mask=mask)
    return back

size = (1000, 600)

cars = ['porsche 718 boxter', 'porsche 718 boxter gts', 'porsche 718 boxter s', 'porsche 718 cayman gts', 'porsche 718 cayman s', 'porsche 911 carrera 4s', 'porsche 911 carrera s', 'porsche macan s']
formats = ['.jpg', '.jpeg', '.png']



for folder in cars:
    images = glob.glob(folder + "/*")

    for image in images:
        with open(image, 'rb') as file:
            file_extension = os.path.splitext(file.name)
            if file_extension[1] in formats:
                img = Image.open(file)
                fit_and_resized_image = ImageOps.fit(img, size, Image.ANTIALIAS)
                fit_and_resized_image.save(file.name)
                position = ((fit_and_resized_image.width - drop(logo).width), 0)
                position2 = (0, 0)
                fit_and_resized_image.paste(drop(logo), position)
                fit_and_resized_image.paste(drop(logo2), position2)
                fit_and_resized_image.save(file.name, dpi=(72, 72))
            else:
                print(file.name + " is not in a known format")
                continue